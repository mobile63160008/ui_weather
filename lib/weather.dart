import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    title: Text(
      "Tokyo",
      style: TextStyle(fontSize: 40, fontWeight: FontWeight.w400),
    ),
    centerTitle: true,
    backgroundColor: Colors.transparent,
    elevation: 0,
  );
}

Widget buildBodyWidget() {
  return Stack(
    children: <Widget>[
      Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.blue,
            image: DecorationImage(
                image: new NetworkImage(
                    "https://external-preview.redd.it/JI_xq_yzYB2iAGfZ_nWV0pw6WHIHnYsd7Oi-GmDMRgQ.jpg?width=640&crop=smart&auto=webp&s=b51f1f482e788c941f37d99d914b34b74b20cf29"),
                fit: BoxFit.fill),
          ),
          child: ListView(
            children: <Widget>[
              buildTemp(),
              buildTime(),
              buildDay(),
            ],
          )),
    ],
  );
}

Widget buildTemp() {
  return Container(
    height: 180,
    child: ListTile(
      title: Text(
        "3°",
        style: TextStyle(
            fontSize: 90, color: Colors.white, fontWeight: FontWeight.w200),
        textAlign: TextAlign.center,
      ),
      subtitle: Text(
        "Mostly Clear\n H:9°  L:0°",
        style: TextStyle(fontSize: 22, color: Colors.white),
        textAlign: TextAlign.center,
      ),
    ),
  );
}

Widget buildTime() {
  return Container(
      margin: EdgeInsets.all(8),
      decoration: ShapeDecoration(
          color: Colors.black26,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))),
      height: 175,
      child: Padding(
        padding: EdgeInsets.all(15),
        child: ListView(
          children: <Widget>[
            Text(
              "Partly cloudy conditions expected around 02:00.",
              style: TextStyle(fontSize: 18, color: Colors.grey.shade300),
            ),
            Divider(
              color: Colors.grey,
            ),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  buildNow(),
                  buildTime2(),
                  buildTime3(),
                  buildTime4(),
                  buildTime5(),
                  buildTime6()
                ],
              ),
            )
          ],
        ),
      ));
}

Widget buildNow() {
  return Column(
    children: <Widget>[
      Text(
        "Now",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      IconButton(
        icon: Icon(
          Icons.nightlight_round_sharp,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "3°",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget buildTime2() {
  return Column(
    children: <Widget>[
      Text(
        "02",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay_rounded,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "2°",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget buildTime3() {
  return Column(
    children: <Widget>[
      Text(
        "03",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay_rounded,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "2°",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget buildTime4() {
  return Column(
    children: <Widget>[
      Text(
        "04",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay_rounded,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "1°",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget buildTime5() {
  return Column(
    children: <Widget>[
      Text(
        "05",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay_rounded,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "1°",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget buildTime6() {
  return Column(
    children: <Widget>[
      Text(
        "06",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay_rounded,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text(
        "1°",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    ],
  );
}


Widget buildDivider(){
  return Divider(
    color: Colors.grey,
  );
}




Widget buildDay() {
  return Container(
      margin: EdgeInsets.all(8),
      decoration: ShapeDecoration(
          color: Colors.black26,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))),
      height: 1000,
      child: Padding(
        padding: EdgeInsets.all(15),
        child: ListView(
          children: <Widget>[
            Text(
              "10-DAY FORECAST",
              style: TextStyle(fontSize: 15, color: Colors.grey.shade600),
            ),
            Divider(
              color: Colors.grey,
            ),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  buildToday(),
                  buildDivider(),
                  buildThu(),
                  buildDivider(),
                  buildFri(),
                  buildDivider(),
                  buildSat(),
                  buildDivider(),
                  buildSun(),
                  buildDivider(),
                  buildMon(),
                  buildDivider(),
                  buildTue(),
                  buildDivider(),
                  buildWed(),
                  buildDivider(),
                  buildThu2(),
                  buildDivider(),
                  buildFri2(),

                ],
              ),
            )
          ],
        ),
      ));
}

Widget buildToday() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Today ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "0°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "10°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildThu() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Thu    ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "1°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "9°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildFri() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Fri       ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.wb_cloudy_rounded,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "-1°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "9°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildSat() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Sat     ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.wb_cloudy_rounded,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "1°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "8°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildSun() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Sun     ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "2°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "12°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildMon() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Mon    ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "2°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "11°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildTue() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Tue     ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.air,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "3°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "11°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildWed() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Wed    ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "1°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "10°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildThu2() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Thu     ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "0°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "12°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}
Widget buildFri2() {
  return Container(
    margin: EdgeInsets.all(4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          "Fri       ",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          "1°",
          style: TextStyle(
              fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.linear_scale_outlined, color: Colors.lightBlueAccent.shade100),
        ),
        Text(
          "14°",
          style: TextStyle(fontSize: 18,color: Colors.white, fontWeight: FontWeight.w800),
        ),
      ],
    ),
  );
}